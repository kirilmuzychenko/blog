<?php
session_start();

if (!empty($_POST)) {
    If (isset($_POST['login']) and isset($_POST['password'])) {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $password = trim($password);
        $password = md5($password);


        require_once "controller/blogUserController.php";
        $check = new userController();
        $data = $check->checkLogin($login);

        $tempLogin = $data['username'];
        $tempPass = $data['password'];
        $tempPass = trim($tempPass);
        $tempRole = $data['role'];

        If ($tempLogin == $login and $tempPass == $password) {
            $_SESSION['username'] = $login;
            $_SESSION['role'] = $tempRole;
            header("Location: blog.php");
            die();
        } else {
            $error = "Incorrect login or password";
            $_SESSION['error'] = $error;
        }

    }
}

require_once "view/header.php";
?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form action="index.php" method="post">
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input name="login" type="text" class="form-control" placeholder="Login">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input name="password" type="password" class="form-control" placeholder="Password">
                        </div>
                        <button name="send" type="submit" class="btn btn-default">Enter</button>
                    </form>
                    <?php
                        if (!empty($_SESSION)){
                            if (isset($_SESSION['error'])){
                                echo $_SESSION['error'];
                                unset($_SESSION['error']);
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </main>

<?php
require_once "view/footer.php";
?>