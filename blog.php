<?php
session_start();

if (isset($_GET['do']) && $_GET['do'] == 'exit') {
    session_unset();
}

if (!isset($_SESSION['username'])) {
    header("Location: index.php");
    die('Please login');
}


require_once "view/header.php";
?>


    <main>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo "Welcome, {$_SESSION['username']}! ";
                    echo "<a class=\"btn btn-default a-profile\" href='blog.php?do=exit' role=\"button\">Logout</a>";
                    If (!empty($_SESSION)) {
                        if ($_SESSION['role'] == 'admin') {
                            echo "<a class=\"btn btn-default a-profile\" href='profile.php' role=\"button\">Profile</a>";
                        }
                    }

                    ?>
                    <h1>Blog</h1>
                    <?php
                    require_once "controller/blogController.php";
                    $printPosts = new blog();
                    $data = $printPosts->getAllPosts();

                    foreach ($data as $post) {
                        echo "<p class='post-title'>" . $post['title'] . "</p>";
                        echo "<p class='post-excert'>" . $post['excert'] . "</p>";
                        echo "<p class='post'>" . $post['post'] . "</p>";
                        echo "<p class='post-author'>" . $post['author'] . "</p>";
                        echo "<p class='post-date'>" . $post['date'] . "</p>";
                        echo "<hr>";
                    }
                    ?>
                    <iframe src="view/comment.php" name="frame"   >��� ������� �� ������� iframe</iframe>
                </div>
            </div>
        </div>
    </main>

<?php
require_once "view/footer.php";
?>