<?php
session_start();

if (!empty($_POST)) {
    If (isset($_POST['newUser']) and isset($_POST['newPass']) and isset($_POST['rePass']) ) {

        $newUser = $_POST['newUser'];
        $newPass = $_POST['newPass'];
        $rePass = $_POST['rePass'];
        $role = $_POST['role'];
        if(iconv_strlen($newUser) > 3 and iconv_strlen($newUser) < 17){
            if(iconv_strlen($newPass) > 3 and iconv_strlen($newPass) < 17) {

                $newPass = trim($newPass);
                $rePass = trim($rePass);

                if ($newPass == $rePass) {
                    $newPass = md5($newPass);
                    require_once "controller/blogUserController.php";
                    $addNewUser = new userController();
                    $tempUser =$addNewUser->checkLogin($newUser);

                    $chkUser = $tempUser['username'];

                    if (!$chkUser == $newUser){
                        $addNewUser->addUser($newUser, $newPass, $role);
                    }
                    else{
                        echo "This user alredy exist";
                    }
                } else {
                    $userError = "Passwords do not match!";
                    $_SESSION['userError'] = $userError;
                }
            }else{
                echo 'Password must have more than 3 letters and must have less than 17 letters';
            }
        }else{
            echo 'Login name must have more than 3 letters must have less than 17 letters';
        }

    }
    if (isset($_POST['title']) and isset($_POST['excert']) and isset($_POST['post'])){
        $title = $_POST['title'];
        $excert = $_POST['excert'];
        $post = $_POST['post'];
        $author = $_SESSION['username'];
        $postDate = date("Y-m-d H:i:s");

        require_once 'controller/blogController.php';
        $addPost = new blog();
        $addPostResult = $addPost->addPost($author,$postDate, $title,$excert, $post);

        If ($addPostResult == 1){
            $postSuccess = "Post successfully added";
            $_SESSION['added-post'] = $postSuccess;
        }
        else{
            $postFail = "Error post not added";
            $_SESSION['added-post-error'] = $postSuccess;
        }
    }
}

require_once "view/header.php";
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Admin's Profile</h1>
                <?php
                echo "Welcome, {$_SESSION['username']}! ";
                echo "<a class=\"btn btn-default a-profile\" href='blog.php?do=exit' role=\"button\">Logout</a>";
                echo "<a class=\"btn btn-default a-profile\" href='blog.php' role=\"button\">Blog</a>";
                ?>
                <h2>Add new user</h2>
                <form action="profile.php" method="post">
                    <div class="form-group">
                        <label for="newUser">Login</label>
                        <input name="newUser" type="text" class="form-control"  placeholder="Enter new user login">
                    </div>
                    <div class="form-group">
                        <label for="newPass">Password</label>
                        <input name="newPass" type="password" class="form-control"  placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="rePass">Retype-Password</label>
                        <input name="rePass" type="password" class="form-control"  placeholder="Retype-Password">
                    </div>
                    <div class="sel">
                        <select class="form-control" name="role">
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                    <button name="send" type="submit" class="btn btn-default">Add user</button>
                </form>
                <?php
                if (!empty($_SESSION)){
                    if (isset($_SESSION['userError'])){
                        echo $_SESSION['userError'];
                        unset($_SESSION['userError']);
                    }
                }
                ?>
                <hr>
                <h2>Add new post</h2>
                <form action="profile.php" method="post">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input name="title" type="text" class="form-control"  placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="excert">Excert</label>
                        <textarea name="excert" class="form-control" rows="3" placeholder="Excert"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="post">Post</label>
                        <textarea name="post" class="form-control" rows="3" placeholder="Enter all text..."></textarea>
                    </div>
                    <button name="send-post" type="submit" class="btn btn-default">Add Post</button>
                </form>
                <?php
                    if (!empty($_SESSION)){
                        if (isset($_SESSION['added-post'])){
                            echo $_SESSION['added-post'];
                            unset($_SESSION['added-post']);
                        }
                        if (isset($_SESSION['added-post-error'])){
                            echo $_SESSION['added-post-error'];
                            unset($_SESSION['added-post-error']);
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</main>
<?php
require_once "view/footer.php";
?>
