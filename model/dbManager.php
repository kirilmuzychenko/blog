<?php
require_once "config.php";

class DB_Manager{
    private $mysqli;

    public function __construct() {
        $this->mysqli = new mysqli(HOST, DBNAME, USERNAME, PASSWORD) or die("Unable to connect");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    public function checkBlogUser($login){
        $login_result = $this->mysqli->query("SELECT * FROM `blogUsers` WHERE `username` = '$login' ");
        return $login_result;
    }

    public function checkUser($login){
        $login_result = $this->mysqli->query("SELECT * FROM `users` WHERE `username` = '$login' ");
        return $login_result;
    }

    public function addBlogUser($username, $password, $role)
    {
        $this->mysqli->query("INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES (NULL, '$username', '$password', '$role')");
    }

    public function addNewPost($author,$postDate, $title, $excert, $post){
       $addResult =  $this->mysqli->query("INSERT INTO `blog` (`id`, `author`, `date`, `title`, `excert`, `post`) VALUES (NULL, '$author', '$postDate', '$title', '$excert', '$post')");
       return $addResult;
    }

    public function getPosts(){
        $getPosts = $addResult =  $this->mysqli->query("SELECT * FROM `blog`");
        return$getPosts;
    }

    public function getComments($startFrom)
    {
        $limit = intval  ($startFrom);
        $result = $this->mysqli->query("SELECT * FROM `comments` ORDER BY `id` DESC LIMIT {$limit}, 5");
        return $result;
    }

    public function addNewUser($login, $password)
    {
        $newUserResult = $this->mysqli->query("INSERT INTO users (login, password) VALUES ('$login', '$password')");
        return $newUserResult;
    }

    public function searchLogin ($login)
    {
        $result = $this->mysqli->query("SELECT `login` FROM `users` WHERE `login`='$login' ");
        return $result;
    }

    public function checkLogin ($login)
    {
        $login_result = $this->mysqli->query("SELECT * FROM `users` WHERE `login` = '$login' ");
        return $login_result;
    }

    public function addNewComment($author, $new_comment)
    {
        $this->mysqli->query("INSERT INTO comments (author, comment) VALUES ('$author', '$new_comment')");
    }

    public function delComment($del_comment)
    {
        $this->mysqli->query("DELETE FROM comments WHERE id = $del_comment");
    }

    public function replaceComment($replace_id, $replace_comment)
    {
        $this->mysqli->query("UPDATE comments SET comment = '$replace_comment' WHERE id = '$replace_id' ");
    }

    public function __destruct() {
        if ($this->mysqli) {
            $this->mysqli->close();
        }
    }

}