<?php
require_once "model/dbManager.php";

class userController{

    public function checkLogin($login){
        $DB = new DB_Manager();
        $user = $DB->checkBlogUser($login);
        $data = mysqli_fetch_assoc($user);
        return $data;
    }

    public function addUser($username, $password, $role){
        $this->$username = $username;
        $this->$password = $password;
        $this->$role = $role;
        $DB = new DB_Manager();
        $DB->addBlogUser($username, $password, $role);
    }


}