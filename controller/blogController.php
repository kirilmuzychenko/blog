<?php

require_once "model/dbManager.php";

class blog{
    function addPost($author,$postDate, $title,$excert, $post){
        $this->$author = $author;
        $this->$postDate = $postDate;
        $this->$title = $title;
        $this->$excert = $excert;
        $this->$post = $post;

        $addNewPost = new DB_Manager();
        $queryResult = $addNewPost->addNewPost($author,$postDate, $title,$excert, $post);
        return $queryResult;
    }

    function  getAllPosts(){
        $getPosts = new DB_Manager();
        $postData = $getPosts->getPosts();
        return $postData;
    }
}